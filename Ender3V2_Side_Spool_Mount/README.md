# Side Spool Mount

Some attempts to create a side spool mount for the Ender 3 V2 3D printer.

## First design proposal

![V1](images/V1.png)

## Second design proposal

![V2](images/V2.png)

## Third design proposal

![V3](images/V3.png)

## Photos

![1](images/1.jpg)

![2](images/2.jpg)

![3](images/3.jpg)


### TODO list
1. Make parametric all designs
2. Reduce the number of sketches
3. Create drawings
4. Make simulations
5. Make assemblies
