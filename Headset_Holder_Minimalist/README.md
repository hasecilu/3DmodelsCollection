# Headphone holder

Small headphone holder to attach it into a thin shelf. It features a cable-holder for those looong cables.

## First design proposal

![V1](images/HeadsetHolder.png)

## Photos

![1](images/1.jpg)

![2](images/2.jpg)

### TODO list
1. Make parametric all designs
2. Reduce the number of sketches
3. Create drawings
4. Make simulations
5. Make assemblies


