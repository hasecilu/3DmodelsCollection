# Mini Spool

First of all, this is based in the idea of CabbitCastle on [Modular Spool for Cables, Wires, Rope, String, Lace, etc.](https://www.thingiverse.com/thing:5023560).

My version is intended for wrapping wire, 30 AWG size. I printed all the parts in PLA with 0.125mm layer height.

The parts are:

* Axis
* Side

You need 2 "Side" parts and one "Axis", aditionally you may need to make a hole with a drill passing both "Side" and "Axis" parts in order to secure the start of the wire.


## Design proposal

### Axis

![Axis](images/Axis.png)

### Side

![Side](images/Side.png)

### Asembly

![Assembly](images/WireSpoolM24.png)

### Screw info

#### Small

The screw is M8. It was created using the Fasteners workbench.

Take these considerations:

* The male screw is at 98% of it's real size, it is smaller.
* The female screw is created substracting (boolean operation) a male at 102% of it's real size, it is bigger.

Using the previous considerations, the pieces printed with PLA with a 0.4 mm diameter nozzle fit one which each other.

#### Big

The screw is M24. It was created using the Fasteners workbench.

Take these considerations:

* The male screw is at 99% -0.05 mm of it's real size, it is smaller.
* The female screw is created substracting (boolean operation) a male at 108% +0.1 mm of it's real size, it is bigger.

Using the previous considerations, the pieces printed with PLA with a 1.0 mm diameter nozzle fit one which each other.

### Raw parts

![Spools](images/Spools.jpg)

![Spoolsx8](images/Spoolsx8.jpg)

### Final result

![Rewinded Spools](images/SpoolsWire.jpg)

![Big spool](images/RGBK.jpg)
