# Nozzle organizer

Small nozzle organizer for small plastic case. 

Some nozzle vendors give you a small plastic case and plastic bags with nozzles inside, everything is a caos. This organizer will help you to have a clean nozzle case.

This design was inspired by the brood comb shape.

It was designed using the KISS principle.

## Design proposal

![Design 0](images/Nozzle_org.png)

![Design 1](images/Nozzle_org_ovrftrd-1.png)

![Design 2](images/Nozzle_org_ovrftrd-2.png)

## Photos

### Before

![1](images/1.jpg)

### After

![2](images/2.jpg)

### Tip

![3](images/3.jpg)


