# Small 3D models collection

This is a simple collection of 3D models that I have designed for 3D printing.

Designed with [FreeCAD](https://www.freecadweb.org/), 
especially the [FreeCAD Link Branch](https://github.com/realthunder/FreeCAD_assembly3) from realthunder <3.

[Download here](https://gitlab.com/hasecilu/3DmodelsCollection/-/archive/main/3DmodelsCollection-main.zip)

## Side Spool Mount

Some attempts to create a side spool mount for the Ender 3 V2 3D printer.


## Headphone Holder

Small headphone holder to attach it into a thin shelf. It features a cable-holder for those looong cables.


## Spool Holder

Nice spool holder to use in Ender 3 V2 machine. It needs a metal rod, 2 bearings and 2 screws.


## Mini Spool

This is a modular Spool for very thin wires, particullarly for wrapping wire, 30 AWG size.

## 2020 profile joint

Small piece to join two profile bars.

## Nozzle organizer

Small organizer for a small plastica case.


