# Spool holder

First of all, this is a remix of [Spool Holder](https://cutt.ly/wRCoshs) by Tek_3D.

This spool holder is for replacing the Creality CR-10 / Ender 3 type of spool holders. It centers the filament spool and has bearings for smooth rotation.

I printed all the parts in PLA .125mm layer height.

The only significative changes in this version are in:

* Holder
* Spool Nut

Instead of having 4 holes now it has 5 holes. See the 3D models or the images below.

## Material

* The 5 3D-printed parts
  - Collar
  - Flange
  - Flange Nut
  - Spool Nut
  - Holder
* 8mm x 150mm Stainless Steel Precision Shafting x1
* 8mm ID x 22mm OD Non-Flanged Ball Bearing x2
* M5x8 screw x2

## Design proposal

![Spool Holder](images/SpoolHolder_5.png)

## Photos

### Printed parts

![Printed Parts](images/PrintedParts.jpg)

### Final result

![Final 1](images/Final1.jpg)

![Final 2](images/Final2.jpg)




